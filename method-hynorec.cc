/* Copyright (C) 2012 Free Software Foundation, Inc.
   Contributed by Torvald Riegel <triegel@redhat.com>.

   This file is part of the GNU Transactional Memory Library (libitm).

   Libitm is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Libitm is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   Under Section 7 of GPL version 3, you are granted additional
   permissions described in the GCC Runtime Library Exception, version
   3.1, as published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License and
   a copy of the GCC Runtime Library Exception along with this program;
   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
   <http://www.gnu.org/licenses/>.  */

#include "libitm_i.h"
#include <immintrin.h>
#include <stdio.h>

/**
 *       This is a trial Hybrid NOrec.
 *
 *       This looks like NOrec in RSTM.
 *       We use wslite to save <addr, value> pairs in reads.
 *
 *       The redolog is a list, which is of course highly inefficient.  The
 *       list doesn't handle casting and alignment issues as it should.  This
 *       should be thought of as experimental code only.
 */


using namespace GTM;

namespace {
    // This group consists of all TM methods that synchronize via multiple locks
    // (or ownership records).
    struct hynorec_stm : public method_group
    {
        // [mfs] LOCK_BIT is 0x800000...000, e.g., MSB 1, rest of bits zero
        static const gtm_word LOCK_BIT = (~(gtm_word)0 >> 1) + 1;
        // Maximum time is all bits except the lock bit, and overflow reserve bit
        static const gtm_word TIME_MAX = (~(gtm_word)0 >> 2);
        // The overflow reserve bit is the MSB of the timestamp part of an orec,
        // so we can have TIME_MAX+1 pending timestamp increases before we overflow.
        static const gtm_word OVERFLOW_RESERVE = TIME_MAX + 1;


      // The shared time base.
      atomic<gtm_word> time __attribute__((aligned(HW_CACHELINE_SIZE)));
      char tailpadding[HW_CACHELINE_SIZE - sizeof(atomic<gtm_word>*)];

      virtual void init()
      {
          printf("Initializing Hynorec_Stm algorithm\n");
          // This store is only executed while holding the serial lock, so relaxed
          // memory order is sufficient here.
          time.store(0, memory_order_relaxed);
      }

      virtual void fini()
      {
      }

      // We only re-initialize when our time base overflows.  Thus, only reset
      // the time base and the orecs but do not re-allocate the orec array.
      //
      // [mfs] This should never happen in 64-bit code...
      virtual void reinit()
      {
          // This store is only executed while holding the serial lock, so relaxed
          // memory order is sufficient here.  Same holds for the memset.
          time.store(0, memory_order_relaxed);
      }
  };

  // [mfs] This is our method group object.  Note that GCC does not have
  //       per-thread pointers.
  static hynorec_stm o_hynorec_stm;

  // [mfs] This is now a write-back algorithm.  Stale text follows
  //
  //
  // [mfs] Note: for now our hack of this is quite gross, and entails doing
  // the lock but not the undo logging
    class hynorec_stm_dispatch : public abi_dispatch // abi_dispatch is defined in dispatch.h
  {
  public:
      hynorec_stm_dispatch() : abi_dispatch(false, true, false, false, 0, &o_hynorec_stm)
      {
      }
  protected:
      hynorec_stm_dispatch(bool ro, bool wt, bool uninstrumented,
                         bool closed_nesting, uint32_t requires_serial, method_group* mg) :
          abi_dispatch(ro, wt, uninstrumented, closed_nesting, requires_serial, mg)
      { }

      static void pre_write(gtm_thread *tx, const void *addr, size_t len)
      {
          return;
      }

      // [mfs] I don't see why we need this dispatch method...  Long-term
      //       todo of removing it.
      static void pre_write(const void *addr, size_t len)
      {
          gtm_thread *tx = gtm_thr();
          pre_write(tx, addr, len);
      }

      // Hynorec_Stm validation
      static gtm_word validate(gtm_thread *tx)
      {
          while (true) {
              // read the lock until it is even
              gtm_word s = o_hynorec_stm.time.load(memory_order_acquire);

              if ((s & 1) == 1)
                  continue;

              // check the read value set
              // don't branch in the loop---consider it backoff if we fail
              // validation early
              bool valid = tx->valuelog.valuecheck();

              if (!valid)
                  // does not allow -1, gtm_word is unsigned int
                  return -1;// [mfs] was 0xFFFFFFFFFFFFFFFFLL;

              // CFENCE;
              // extend start time
              tx->shared_state.store(s, memory_order_release);
              if (o_hynorec_stm.time.load(memory_order_acquire) == s)
                  return s;
          }
      }

      //[wer] In NOrec, extending start time is embedded in validation
      // Tries to extend the snapshot to a more recent time.  Returns the new
      // snapshot time and updates TX->SHARED_STATE.  If the snapshot cannot be
      // extended to the current global time, TX is restarted.
      static gtm_word extend(gtm_thread *tx)
      {
          // We read global time here, even if this isn't strictly necessary
          // because we could just return the maximum of the timestamps that
          // validate sees.  However, the potential cache miss on global time is
          // probably a reasonable price to pay for avoiding unnecessary extensions
          // in the future.
          //
          // We need acquire memory oder because we have to synchronize with the
          // increment of global time by update transactions, whose lock
          // acquisitions we have to observe (also see trycommit()).
          gtm_word snapshot = o_hynorec_stm.time.load(memory_order_acquire);
          if (validate(tx) == (gtm_word)-1)
              tx->restart(RESTART_VALIDATE_READ);

          // Update our public snapshot time.  Probably useful to decrease waiting
          // due to quiescence-based privatization safety.
          // Use release memory order to establish synchronizes-with with the
          // privatizers; prior data loads should happen before the privatizers
          // potentially modify anything.

          // [mfs] An interesting point here is that since validate() does
          //       not look at shared_state, we could update shared_state
          //       *before* the call to validate.  This would not affect
          //       correctness, but might decrease quiescence time,
          //       especially for large doomed transactions or when using the
          //       lazy trick discussed above.
          tx->shared_state.store(snapshot, memory_order_release);
          return snapshot;
      }


      static gtm_rwlog_entry* pre_load(gtm_thread *tx, const void* addr,
                                       size_t len)
      {
          return NULL;
      }


      static void post_load(gtm_thread *tx, gtm_rwlog_entry* log)
      {
          return;
      }

      template <typename V> static V op_read(const V* addr, ls_modifier mod)
      {
                    gtm_thread *tx = gtm_thr();
          //if (mod == RfW)
          //printf("what? ");

          //[wer] <1> check redolog
          // cheap and dirty... we're just going to get the entry in the
          // writeset, if one exists...
          //wsentry_t* e = tx->redolog.find(addr);
          WriteSetEntry e;
          wslog_insert_data<V>(&e, const_cast<V*>(addr), 0);
          bool found = tx->redolog.find(e);

          // [mfs] Note that this will not be valid when we implement a
          //       solution that properly handles overlapping accesses of
          //       varying granularity.
          //          if (e) {
          if (found) {
              return wslog_extract_data<V>(e, addr);
          }

          //[wer] <3> regular norec read-ro function
          // A read is valid iff it occurs during a period where the seqlock does
          // not change and is even.  This code also polls for new changes that
          // might necessitate a validation.
          V value = *addr;

          // CFENCE;
          // get start time, compared to the current timestamp
          gtm_word start_time = tx->shared_state.load(memory_order_acquire);
          while (start_time != o_hynorec_stm.time.load(memory_order_acquire)) {
              if ((start_time = validate(tx)) == (gtm_word)-1) {
                  tx->conf_abort_total++;
                  tx->restart(RESTART_VALIDATE_READ);
              }//Abort

              value = *addr;
              //CFENCE;
          }

          // log address and value into read value log
          WriteSetEntry t;
          // [wer] is it dangerous to use const_cast here?
          wslog_insert_data<V>(&t, const_cast<V*>(addr), value);
          tx->valuelog.insert(t);

          return value;
      }

      template <typename V> static void op_write(V* addr, const V value,
                                              ls_modifier mod)
      {
      }

      template <typename V> static V load(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          //[wer] <1> check redolog
          // cheap and dirty... we're just going to get the entry in the
          // writeset, if one exists...
          //wsentry_t* e = tx->redolog.find(addr);
          WriteSetEntry e;
          wslog_insert_data<V>(&e, const_cast<V*>(addr), 0);
          bool found = tx->redolog.find(e);

          // [mfs] Note that this will not be valid when we implement a
          //       solution that properly handles overlapping accesses of
          //       varying granularity.
          //          if (e) {
          if (found) {
              return wslog_extract_data<V>(e, addr);
          }

          //[wer] <3> regular norec read-ro function
          // A read is valid iff it occurs during a period where the seqlock does
          // not change and is even.  This code also polls for new changes that
          // might necessitate a validation.
          V value = *addr;

          // CFENCE;
          // get start time, compared to the current timestamp
          gtm_word start_time = tx->shared_state.load(memory_order_acquire);
          while (start_time != o_hynorec_stm.time.load(memory_order_acquire)) {
              if ((start_time = validate(tx)) == (gtm_word)-1) {
                  tx->conf_abort_total++;
                  tx->restart(RESTART_VALIDATE_READ);
              }//Abort

              value = *addr;
              //CFENCE;
          }

          // log address and value into read value log
          WriteSetEntry t;
          // [wer] is it dangerous to use const_cast here?
          wslog_insert_data<V>(&t, const_cast<V*>(addr), value);
          tx->valuelog.insert(t);

          return value;
      }

      template <typename V> static void store(V* addr, const V value,
                                              ls_modifier mod)
      {
          // [mfs] the current interface here is fine, if somewhat suboptimal
          //       due to multi-copying.  I haven't determined whether this
          //       is getting inlined sufficiently to avoid the cost
          gtm_thread *tx = gtm_thr();
          // save the data into redo log
          //wsentry_t e;
          WriteSetEntry e;
          wslog_insert_data<V>(&e, addr, value);
          tx->redolog.insert(e);
      }

      //[wer210] Original NOrec does not support ops()
      template <typename V> static void ops(V* addr, const V value,
                                            ls_modifier mod, int op)
      {
          assert (false && "Hynorec_Stm does not support ops(), use algorithm hynorec_stm_op instead.");
      }

    public:
      static void memtransfer_static(void *dst, const void* src, size_t size,
                                     bool may_overlap, ls_modifier dst_mod, ls_modifier src_mod)
      {
          //[wer] copy "num of size" bytes, so simply treat the type as unsigned char
          // we also do not care about overlap, as we do buffered write.
          unsigned char* srcaddr = (unsigned char*)const_cast<void*>(src);
          unsigned char* dstaddr = (unsigned char*)dst;

          // load and store
          for (size_t i = 0; i < size; i++) {
              unsigned char temp = load<unsigned char>(srcaddr, RaR);
              store<unsigned char>(dstaddr, temp, WaW);
              dstaddr = (unsigned char*) ((long long)dstaddr + sizeof(unsigned char));
              srcaddr = (unsigned char*) ((long long)srcaddr + sizeof(unsigned char));
          }
      }

      static void memset_static(void *dst, int c, size_t size, ls_modifier mod)
      {
          gtm_thread* tx = gtm_thr();
          unsigned char* dstaddr = (unsigned char*)dst;

          // save data into redo log
          for(size_t it = 0; it < size; it++) {
              WriteSetEntry e;
              //wsentry_t e;
              wslog_insert_data<unsigned char>(&e, dstaddr, (unsigned char)c);
              tx->redolog.insert(e);
              dstaddr = (unsigned char*) ((long long)dst + sizeof(unsigned char));
          }
      }

      // [wer] norec begin function
      virtual gtm_restart_reason begin_or_restart()
      {
          // We don't need to do anything for nested transactions.
          gtm_thread *tx = gtm_thr();

          if (tx->parent_txns.size() > 0)
              return NO_RESTART;

          // Read the current time, which becomes our snapshot time.
          // Use acquire memory oder so that we see the lock acquisitions by update
          // transcations that incremented the global time (see trycommit()).
          //[wer] gtm_word is unsigned int, time starts from 0, and then 1,2,3...
#if 0
          uint32_t status;
          // [wer] HTM_POST_BEGIN
          if ((status = _xbegin()) == _XBEGIN_STARTED) {
              //[wer210] TODO use volatile for time?
              gtm_word snapshot = o_hynorec_stm.time.load(memory_order_acquire);
              if (snapshot & 1) {
                  _xabort(_XABORT_CONFLICT);
                  return RESTART_LOCKED_WRITE;
              }
              gtm_thread *t = gtm_thr();
              // save the start time (no ordering needed)
              t->shared_state.store(snapshot, memory_order_relaxed);
              return NO_RESTART;
          }
          else {
#endif
              gtm_word snapshot = o_hynorec_stm.time.load(memory_order_acquire);
              // Sample the sequence lock, if it is even decrement by 1
              snapshot = snapshot & ~(1L);

              // Re-initialize method group on time overflow.
              if (snapshot >= o_hynorec_stm.TIME_MAX)
                  return RESTART_INIT_METHOD_GROUP;

              // save the start time (no ordering needed)
              tx->shared_state.store(snapshot, memory_order_relaxed);
              return NO_RESTART;
              //}
      }

      virtual bool trycommit(gtm_word& priv_time)
      {
          gtm_thread* tx = gtm_thr();
          gtm_word start_time = 0;
          // [read-only transactions]
          // If we haven't updated anything, we can commit. Just clean value log.
          if (!tx->redolog.size()) {
                  tx->valuelog.reset();
                  return true;
          }

          // [read-write transactions]
          // get start time
          start_time = tx->shared_state.load(memory_order_relaxed);

          // get the lock and validate
          // [compare_exchange_weak] should save some overhead in a loop?
          while (!o_hynorec_stm.time.compare_exchange_weak
                 (start_time, start_time + 1, memory_order_acquire)) {
              if ((start_time = validate(tx)) == (gtm_word)-1) {
                  tx->conf_abort_total++;
                  return false;
              }
          }

          // do write back
          if (tx->redolog.size())
              tx->redolog.writeback();

          //CFENCE;
          //atomic_thread_fence(memory_order_release);

          // relaese the sequence lock
          gtm_word ct = start_time + 2;
          o_hynorec_stm.time.store(ct, memory_order_release);

          // We're done, clear the logs.
          tx->writelog.clear();
          tx->valuelog.reset();
          tx->redolog.reset();

          // Need to ensure privatization safety. Every other transaction must
          // have a snapshot time that is at least as high as our commit time
          // (i.e., our commit must be visible to them).
          priv_time = ct;
          return true;
      }

      virtual void rollback(gtm_transaction_cp *cp)
      {
          if (cp != 0)
              return;

          gtm_thread *tx = gtm_thr();


          // We need this release fence to ensure that privatizers see the
          // rolled-back original state (not any uncommitted values) when they read
          // the new snapshot time that we write in begin_or_restart().
          atomic_thread_fence(memory_order_release);

          // We're done, clear the logs.
          tx->writelog.clear();
          tx->redolog.reset();
          tx->valuelog.reset();
      }

      virtual bool supports(unsigned number_of_threads)
      {
          // Each txn can commit and fail and rollback once before checking for
          // overflow, so this bounds the number of threads that we can support.
          // In practice, this won't be a problem but we check it anyway so that
          // we never break in the occasional weird situation.
          return (number_of_threads * 2 <= hynorec_stm::OVERFLOW_RESERVE);
      }

      CREATE_DISPATCH_METHODS(virtual, )
      CREATE_DISPATCH_METHODS_MEM()

      //hynorec_stm_dispatch() : abi_dispatch(false, true, false, false, 0, &o_hynorec_stm)
      //{ }
  };

    ////////////////////////////////////////////////////////////
    // This group is HTM with above Hynorec_stm mode as a fallback.
    struct hynorec : public method_group
    {
        virtual void init()
        {
            printf("Initializing HyNOrec algorithm...\n");
            // Enable the HTM fastpath if the HW is available.  The fastpath is
            // initially disabled.
#ifdef USE_HTM_FASTPATH
            htm_fastpath = htm_init();
            if (htm_fastpath > 0)
                printf ("HTM is available, use HTM now...\n");
            else
                printf ("HTM is unavailable, use software NOrec-mode now...\n");
#endif
        }
        virtual void fini()
        {
            // Disable the HTM fastpath.
            htm_fastpath = 0;
        }
    };

    static hynorec o_hynorec;

    // We just need the subclass to associate it with the HTM method group that
    // sets up the HTM fast path.  This will use serial_dispatch as fallback for
    // transactions that might get canceled; it has a different method group, but
    // this is harmless for serial dispatchs because they never abort.
    class hynorec_dispatch : public hynorec_stm_dispatch
    {
    public:
        hynorec_dispatch() : hynorec_stm_dispatch(false, true, false, false,
                                                  gtm_thread::STATE_SERIAL | gtm_thread::STATE_IRREVOCABLE,
                                                  &o_hynorec)
        { }
    };

} // anon namespace

static const hynorec_stm_dispatch o_hynorec_stm_dispatch;
static const hynorec_dispatch o_hynorec_dispatch;

abi_dispatch *
GTM::dispatch_hynorec_stm ()
{
    return const_cast<hynorec_stm_dispatch *>(&o_hynorec_stm_dispatch);
}

abi_dispatch *
GTM::dispatch_hynorec ()
{
    return const_cast <hynorec_dispatch *>(&o_hynorec_dispatch);
}
