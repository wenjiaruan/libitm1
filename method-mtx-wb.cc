/* Copyright (C) 2012 Free Software Foundation, Inc.
   Contributed by Torvald Riegel <triegel@redhat.com>.

   This file is part of the GNU Transactional Memory Library (libitm).

   Libitm is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Libitm is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   Under Section 7 of GPL version 3, you are granted additional
   permissions described in the GCC Runtime Library Exception, version
   3.1, as published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License and
   a copy of the GCC Runtime Library Exception along with this program;
   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
   <http://www.gnu.org/licenses/>.  */

#include "libitm_i.h"
#include <stdio.h>


// mutex lock, write-back version

using namespace GTM;

namespace {

  volatile int lock = 0;

  // method group with all the operations for method-gl_mtx_wb
  struct gl_mtx_wb_mg : public method_group
  {
      // method-wide definitions.  We have as much overlap with method-ml as
      // is reasonable, deviating only when there is an algorithmic need
      //
      // NB: some of this code is unnecessary, but is preserved for the sake
      //     of consistency with method-ml.  Note, too, that method-ml has
      //     more documentation on /why/ these are implemented as they are.

      // methods for managing lock bits and overflow of the global timebase
      //
      // NB: There are no incarnation bits for this algorithm, since we don't
      //     have undo logs
      static const gtm_word LOCK_BIT = (~(gtm_word)0 >> 1) + 1;
      static const gtm_word TIME_MAX = (~(gtm_word)0 >> 2);
      static const gtm_word OVERFLOW_RESERVE = TIME_MAX + 1;
      static bool is_locked(gtm_word o) { return o & LOCK_BIT; }
      static gtm_word set_locked(gtm_thread *tx)
      {
          return ((uintptr_t)tx >> 1) | LOCK_BIT;
      }
      static gtm_word get_time(gtm_word o) { return o; }
      static gtm_word set_time(gtm_word time) { return time; }
      static bool is_more_recent_or_locked(gtm_word o, gtm_word than_time)
      {
          return get_time(o) > than_time;
      }

      // The shared time base.
      atomic<gtm_word> time __attribute__((aligned(HW_CACHELINE_SIZE)));

      // The array of ownership records.
      atomic<gtm_word>* orecs __attribute__((aligned(HW_CACHELINE_SIZE)));
      char tailpadding[HW_CACHELINE_SIZE - sizeof(atomic<gtm_word>*)];

      // Location-to-orec mapping.  Stripes of 16B mapped to 2^19 orecs.
      static const gtm_word L2O_ORECS = 1 << 19;
      static const gtm_word L2O_SHIFT = 4;
      static size_t get_orec(const void* addr)
      {
          return ((uintptr_t)addr >> L2O_SHIFT) & (L2O_ORECS - 1);
      }
      static size_t get_next_orec(size_t orec)
      {
          return (orec + 1) & (L2O_ORECS - 1);
      }
      static size_t get_orec_end(const void* addr, size_t len)
      {
          return (((uintptr_t)addr + len + (1 << L2O_SHIFT) - 1) >> L2O_SHIFT)
              & (L2O_ORECS - 1);
      }

      virtual void init()
      {
          // for now, we're going to print a message to confirm algorithm selection
          printf("Initializing gl_mtx_wb algorithm\n");
          // NB: this code is called while holding the serial lock
          orecs = (atomic<gtm_word>*)
              xcalloc(sizeof(atomic<gtm_word>) * L2O_ORECS, true);
          time.store(0, memory_order_relaxed);
      }

      virtual void fini()
      {
          free(orecs);
      }

      // We only re-initialize when our time base overflows.  Thus, only reset
      // the time base and the orecs but do not re-allocate the orec array.
      //
      // [mfs] This should never happen in 64-bit code...
      virtual void reinit()
      {
          // This store is only executed while holding the serial lock, so relaxed
          // memory order is sufficient here.  Same holds for the memset.
          time.store(0, memory_order_relaxed);
          memset(orecs, 0, sizeof(atomic<gtm_word>) * L2O_ORECS);
      }
  };

  // [mfs] This is our method group object.  Note that GCC does not have
  //       per-thread pointers.
  static gl_mtx_wb_mg o_gl_mtx_wb_mg;

  // The multiple lock, write-back TM method
  //
  // As discussed above, this should be thought of as (RSTM OrecLazy) +
  // (Privatization Safety via Quiescence).  In particular, this means eager
  // locking with write back.  We never expect this to be a good algorithm.
  //
  // Reads use time-based validation with timestamp extension.
  //
  // gtm_thread::shared_state is used to store a transaction's current
  // snapshot time (or commit time). The serial lock uses ~0 for inactive
  // transactions and 0 for active ones. Thus, we always have a meaningful
  // timestamp in shared_state that can be used to implement quiescence-based
  // privatization safety.
  class gl_mtx_wb_dispatch : public abi_dispatch
  {
    protected:
      template <typename V> static V op_read(const V* addr, ls_modifier mod)
      {
          assert (false && "This is gl_mtx_wb, use gl_mtx_wb_op instead.");
          V v = *addr;
          return v;
      }

      // All _ITM_R* functions ultimately become instances of this
      template <typename V> static V load(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();
          assert (sizeof (V) <= 64);
          V v;
          if (!tx->redolog_bst.isEmpty() && tx->redolog_bst.find(addr, v) != 0)
              return v;

          v = *addr;
          return v;
      }

      template <typename V> static void op_write(V* addr, const V value,
                                                 ls_modifier mod)
      {
          assert (false && "This is gl_mtx_wb, use gl_mtx_wb_op instead.");
      }

      // All _ITM_W functions eventually become instances of this
      template <typename V> static void store(V* addr, const V value,
                                              ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();
          assert (sizeof (V) <= 64);
          // save the data into the redo log
          tx->redolog_bst.insert(addr, value);
      }

      template <typename V> static void ops(V* addr, const V value,
                                            ls_modifier mod, int op)
      {
          assert (false && "This is gl_mtx_wb, use gl_mtx_wb_op instead.");
      }

    public:
      // [mfs] TODO
      static void memtransfer_static(void *dst, const void* src, size_t size,
                                     bool may_overlap, ls_modifier dst_mod, ls_modifier src_mod)
      {
          //It's not optimal, but it should at least work

          //[wer] copy "num of size" bytes, so simply treat the type as unsigned char
          // we also do not care about overlap, as we do buffered write.
          unsigned char* srcaddr = (unsigned char*)const_cast<void*>(src);
          unsigned char* dstaddr = (unsigned char*)dst;

          // load and store
          for (size_t i = 0; i < size; i++) {
              unsigned char temp = load<unsigned char>(srcaddr, RaR);
              store<unsigned char>(dstaddr, temp, WaW);
              dstaddr = (unsigned char*) ((long long)dstaddr + sizeof(unsigned char));
              srcaddr = (unsigned char*) ((long long)srcaddr + sizeof(unsigned char));
          }
      }

      // [mfs] TODO
      static void memset_static(void *dst, int c, size_t size, ls_modifier mod)
      {
          gtm_thread* tx = gtm_thr();
          unsigned char* dstaddr = (unsigned char*)dst;

          // save data into redo log
          for(size_t it = 0; it < size; it++) {
              tx->redolog_bst.insert(dstaddr, (unsigned char)c);
              dstaddr = (unsigned char*) ((long long)dst + sizeof(unsigned char));
          }
      }

      // basic code for starting a transaction
      virtual gtm_restart_reason begin_or_restart()
      {
          // for flat nested transactions, just return...
          //
          // [mfs] TODO: verify that we only have flat nesting, not closed
          //             nesting...
          gtm_thread *tx = gtm_thr();

          // wait for the global lock to start
          while (__sync_val_compare_and_swap (&lock, 0, 1) == 1);

          if (tx->parent_txns.size() > 0)
              return NO_RESTART;

          // Use an ordered read to get transaction start time
          gtm_word snapshot = o_gl_mtx_wb_mg.time.load(memory_order_acquire);
          // deal with time overflow
          if (snapshot >= o_gl_mtx_wb_mg.TIME_MAX)
              return RESTART_INIT_METHOD_GROUP;

          return NO_RESTART;
      }

      // Code for committing a transaction
      virtual bool trycommit(gtm_word& priv_time)
      {
          gtm_thread* tx = gtm_thr();

          // read-only transactions just clear their read log and they're
          // done.  Note that we assume readers don't privatize, so priv_time
          // isn't set
          if (tx->redolog_bst.isEmpty()) {
              __sync_val_compare_and_swap (&lock, 1, 0);
              return true;
          }

          // replay redo log
          tx->redolog_bst.writeback();

          __sync_val_compare_and_swap (&lock, 1, 0);

          // clear logs
          tx->redolog_bst.reset();

          return true;
      }

      // this code runs on abort
      virtual void rollback(gtm_transaction_cp *cp)
      {
          // assuming flat nesting, we just return on abort of nested transaction
          if (cp != 0)
              return;

          gtm_thread *tx = gtm_thr();

          __sync_val_compare_and_swap (&lock ,1, 0);

          // We're done, clear the logs.
          tx->redolog_bst.reset();
      }

      virtual bool supports(unsigned number_of_threads)
      {
          // Each txn can commit and fail and rollback once before checking for
          // overflow, so this bounds the number of threads that we can support.
          // In practice, this won't be a problem but we check it anyway so that
          // we never break in the occasional weird situation.
          return (number_of_threads * 2 <= gl_mtx_wb_mg::OVERFLOW_RESERVE);
      }

      CREATE_DISPATCH_METHODS(virtual, )
      CREATE_DISPATCH_METHODS_MEM()

      gl_mtx_wb_dispatch() : abi_dispatch(false, true, false, false, 0, &o_gl_mtx_wb_mg)
      { }
  };

} // anon namespace

static const gl_mtx_wb_dispatch o_gl_mtx_wb_dispatch;

abi_dispatch *
GTM::dispatch_gl_mtx_wb ()
{
    return const_cast<gl_mtx_wb_dispatch *>(&o_gl_mtx_wb_dispatch);
}
