/* Copyright (C) 2012 Free Software Foundation, Inc.
   Contributed by Torvald Riegel <triegel@redhat.com>.

   This file is part of the GNU Transactional Memory Library (libitm).

   Libitm is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Libitm is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   Under Section 7 of GPL version 3, you are granted additional
   permissions described in the GCC Runtime Library Exception, version
   3.1, as published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License and
   a copy of the GCC Runtime Library Exception along with this program;
   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
   <http://www.gnu.org/licenses/>.  */

#include "libitm_i.h"
#include <stdio.h>

/**
 *  This method is a privatization-safe variant of the RSTM OrecEagerRedo
 *  algorithm.  The implemenation itself is a derivative of method-ml, which
 *  roughly corresponds to a privatization-safe variant of RSTM OrecEager.
 *
 *  The only noteworthy difference between this implementation and method-ml
 *  is that here, we do not use an undo log.  As in method-ml, we acquire
 *  orecs eagerly, upon first write.  However, we log speculative state in a
 *  redo log, which we replay at commit time.
 *
 *  Naturally, this means that we must do a lookup in the log on every read.
 *  Note that for the time being, the redolog is a list, which is of course
 *  highly inefficient.  The list doesn't handle casting and overlapping
 *  accesses as it should.  That is, if I write a byte at address 0x1 to the
 *  log, and then try to read a word at address 0x0, things break.  Likewise,
 *  if I write a byte at address 0x1 and then try to write a word at address
 *  0x0, things break.  These issues are best treated as orthogonal to the
 *  locking mechanism -- in short, we are not using a robust writeset
 *  implementation yet.
 *
 *  This should be thought of as experimental code only.
 */

using namespace GTM;

namespace {

  volatile int l = 0;

  // method group with all the operations for method-oreceagerredo_op
  struct oreceagerredo_op_mg : public method_group
  {
      // method-wide definitions.  We have as much overlap with method-ml as
      // is reasonable, deviating only when there is an algorithmic need
      //
      // NB: some of this code is unnecessary, but is preserved for the sake
      //     of consistency with method-ml.  Note, too, that method-ml has
      //     more documentation on /why/ these are implemented as they are.

      // methods for managing lock bits and overflow of the global timebase
      //
      // NB: There are no incarnation bits for this algorithm, since we don't
      //     have undo logs
      static const gtm_word LOCK_BIT = (~(gtm_word)0 >> 1) + 1;
      static const gtm_word TIME_MAX = (~(gtm_word)0 >> 2);
      static const gtm_word OVERFLOW_RESERVE = TIME_MAX + 1;
      static bool is_locked(gtm_word o) { return o & LOCK_BIT; }
      static gtm_word set_locked(gtm_thread *tx)
      {
          return ((uintptr_t)tx >> 1) | LOCK_BIT;
      }
      static gtm_word get_time(gtm_word o) { return o; }
      static gtm_word set_time(gtm_word time) { return time; }
      static bool is_more_recent_or_locked(gtm_word o, gtm_word than_time)
      {
          return get_time(o) > than_time;
      }

      // The shared time base.
      atomic<gtm_word> time __attribute__((aligned(HW_CACHELINE_SIZE)));

      // The array of ownership records.
      atomic<gtm_word>* orecs __attribute__((aligned(HW_CACHELINE_SIZE)));
      char tailpadding[HW_CACHELINE_SIZE - sizeof(atomic<gtm_word>*)];

      // Location-to-orec mapping.  Stripes of 16B mapped to 2^19 orecs.
      static const gtm_word L2O_ORECS = 1 << 19;
      static const gtm_word L2O_SHIFT = 4;
      static size_t get_orec(const void* addr)
      {
          return ((uintptr_t)addr >> L2O_SHIFT) & (L2O_ORECS - 1);
      }
      static size_t get_next_orec(size_t orec)
      {
          return (orec + 1) & (L2O_ORECS - 1);
      }
      static size_t get_orec_end(const void* addr, size_t len)
      {
          return (((uintptr_t)addr + len + (1 << L2O_SHIFT) - 1) >> L2O_SHIFT)
              & (L2O_ORECS - 1);
      }

      virtual void init()
      {
          // for now, we're going to print a message to confirm algorithm selection
          printf("Initializing oreceagerredo_op algorithm\n");
          // NB: this code is called while holding the serial lock
          orecs = (atomic<gtm_word>*)
              xcalloc(sizeof(atomic<gtm_word>) * L2O_ORECS, true);
          time.store(0, memory_order_relaxed);
      }

      virtual void fini()
      {
          free(orecs);
      }

      // We only re-initialize when our time base overflows.  Thus, only reset
      // the time base and the orecs but do not re-allocate the orec array.
      //
      // [mfs] This should never happen in 64-bit code...
      virtual void reinit()
      {
          // This store is only executed while holding the serial lock, so relaxed
          // memory order is sufficient here.  Same holds for the memset.
          time.store(0, memory_order_relaxed);
          memset(orecs, 0, sizeof(atomic<gtm_word>) * L2O_ORECS);
      }
  };

  // [mfs] This is our method group object.  Note that GCC does not have
  //       per-thread pointers.
  static oreceagerredo_op_mg o_oreceagerredo_op_mg;

  // The multiple lock, write-back TM method
  //
  // As discussed above, this should be thought of as (RSTM Oreceagerredo_Op) +
  // (Privatization Safety via Quiescence).  In particular, this means eager
  // locking with write back.  We never expect this to be a good algorithm.
  //
  // Reads use time-based validation with timestamp extension.
  //
  // gtm_thread::shared_state is used to store a transaction's current
  // snapshot time (or commit time). The serial lock uses ~0 for inactive
  // transactions and 0 for active ones. Thus, we always have a meaningful
  // timestamp in shared_state that can be used to implement quiescence-based
  // privatization safety.
  class oreceagerredo_op_dispatch : public abi_dispatch
  {
    protected:
      static void pre_write(gtm_thread *tx, const void *addr, size_t len)
      {
          // 'snapshot' is my thread's start time... Only I write it, so I
          // can do a relaxed read of it.
          gtm_word snapshot = tx->shared_state.load(memory_order_relaxed);
          gtm_word locked_by_tx = oreceagerredo_op_mg::set_locked(tx);

          // Lock all orecs that cover the region.
          //
          // [mfs] A few notes (first 2 confirmed by Torvald)
          //       1 - is this ever more than 2 locks?  (only if used for
          //           mem* functions)
          //
          //       2 - can orec_end be less than orec?  (yes, and it could be
          //           a bug for mem* functions touching *huge* amounts of
          //           data)
          //
          //       3 - can it ever be >1 lock for an aligned access (I think no)
          //
          //       4 - can it ever be unaligned for non-x86 code? (I think no)
          size_t orec = oreceagerredo_op_mg::get_orec(addr);
          size_t orec_end = oreceagerredo_op_mg::get_orec_end(addr, len);
          do
          {
              // Acquire orec

              // read orec: either we locked it or we're going to lock it, so
              // relaxed order OK
              gtm_word o = o_oreceagerredo_op_mg.orecs[orec].load(memory_order_relaxed);

              // if locked by self, move to next
              if (likely (locked_by_tx != o))
              {
                  // if locked by other, self-abort
                  if (unlikely (oreceagerredo_op_mg::is_locked(o)))
                      tx->restart(RESTART_LOCKED_WRITE);

                  // if unlocked but too new, extend timestamp here
                  //
                  // NB: this causes a validation, which might abort.  Note,
                  // too, that this is not like RSTM: we don't 'continue'
                  // here, instead choosing to hope that the location doesn't
                  // change again during validation.
                  if (unlikely (oreceagerredo_op_mg::get_time(o) > snapshot))
                      snapshot = extend(tx);

                  // Grab the lock with a strong/acquiring CAS, abort on failure
                  if (unlikely (!o_oreceagerredo_op_mg.orecs[orec].
                                compare_exchange_strong(o, locked_by_tx, memory_order_acquire)))
                      tx->restart(RESTART_LOCKED_WRITE);

                  // We may need a release fence here... see method-ml
                  atomic_thread_fence(memory_order_release);

                  // [mfs] Log the orec we just acquired.  Note that we are
                  //       using TinySTM-style orecs instead of IBM-style
                  //       orecs, which means that our log has to keep the
                  //       old value and the orec address
                  gtm_rwlog_entry *e = tx->writelog.push();
                  e->orec = o_oreceagerredo_op_mg.orecs + orec; // NB: pointer to orec
                  e->value = o;
              }
              // move to next orec
              orec = o_oreceagerredo_op_mg.get_next_orec(orec);
          }
          // [mfs] We should be iterating over addresses, not orecs... this
          //       while could be problematic for extremely large
          //       memset/memcpy operations
          while (orec != orec_end);

          // postcondition: all orecs covering this location have been acquired
      }

      // Acquire orecs in opslog(), does not abort upon addr updated after a tx started
      static bool pre_ops(gtm_thread *tx, const void *addr, size_t len)
      {
          // my lock
          gtm_word locked_by_tx = oreceagerredo_op_mg::set_locked(tx);

          // Lock all orecs that cover the region.
          size_t orec = oreceagerredo_op_mg::get_orec(addr);
          size_t orec_end = oreceagerredo_op_mg::get_orec_end(addr, len);
          do {
              // Acquire orec
              gtm_word o = o_oreceagerredo_op_mg.orecs[orec].load(memory_order_relaxed);

              // if locked by self, move to next
              if (likely (locked_by_tx != o))
              {
                  // if locked by other, self-abort
                  if (unlikely (oreceagerredo_op_mg::is_locked(o)))
                      return false;

                  // Grab the lock with a strong/acquiring CAS, spin on failure
                  if (unlikely (!o_oreceagerredo_op_mg.orecs[orec].
                                compare_exchange_strong(o, locked_by_tx, memory_order_acquire)))
                      return false;

                  // We may need a release fence here... see method-ml
                  atomic_thread_fence(memory_order_release);

                  // [mfs] Log the orec we just acquired.  Note that we are
                  //       using TinySTM-style orecs instead of IBM-style
                  //       orecs, which means that our log has to keep the
                  //       old value and the orec address
                  gtm_rwlog_entry *e = tx->writelog.push();
                  e->orec = o_oreceagerredo_op_mg.orecs + orec; // NB: pointer to orec
                  e->value = o;
              }
              // move to next orec
              orec = o_oreceagerredo_op_mg.get_next_orec(orec);
          }
          // [mfs] We should be iterating over addresses, not orecs... this
          //       while could be problematic for extremely large
          //       memset/memcpy operations
          while (orec != orec_end);

          return true;
          // postcondition: all orecs covering this location have been acquired
      }

      // Returns true iff all the orecs in our read log still have the same time
      // or have been locked by the transaction itself.
      //
      // NB: This relies on the above pre_write code never locking a location
      //     whose timestamp is too new
      static bool validate(gtm_thread *tx)
      {
          // figure out our lock word
          gtm_word locked_by_tx = oreceagerredo_op_mg::set_locked(tx);

          // check each readset entry
          //
          // [mfs] it should suffice to compare to tx->shared_state instead of
          //       to i->value, in which case we could avoid having to log orec
          //       locations and values
          for (gtm_rwlog_entry *i = tx->readlog.begin(), *ie = tx->readlog.end();
               i != ie; i++)
          {
              // read orec; see method-ml for justification of ordering
              gtm_word o = i->orec->load(memory_order_relaxed);
              // compare orec value to expected value, fail on similarity
              //
              // [NB] in oreclazy, we could compute a bool instead of
              //      returning early
              if (oreceagerredo_op_mg::get_time(o) != oreceagerredo_op_mg::get_time(i->value)
                  && o != locked_by_tx)
                  return false;
          }
          return true;
      }

      // Timestamp extension code
      static gtm_word extend(gtm_thread *tx)
      {
          // get the current time, since it will become our new start time
          //
          // see method-ml for discussion of acquire ordering
          gtm_word snapshot = o_oreceagerredo_op_mg.time.load(memory_order_acquire);
          if (!validate(tx))
              tx->restart(RESTART_VALIDATE_READ);

          // Update our public snapshot time
          tx->shared_state.store(snapshot, memory_order_release);
          return snapshot;
      }

      // Perform initial check of orecs, and log them
      //
      // [mfs] Early logging could be bad if we extend, but the assumption is
      //       that extension is rare
      static gtm_rwlog_entry* pre_load(gtm_thread *tx, const void* addr,
                                       size_t len)
      {
          // get current size of read log (but not iterator) so we can return
          // pointer to set of orecs added to read set...
          //
          // [mfs] not sure if this complexity is worth it... we could just
          //       re-compute orecs in post_load
          size_t log_start = tx->readlog.size();
          // NB: can do relaxed read of snapshot since there are no remote
          //     writes
          gtm_word snapshot = tx->shared_state.load(memory_order_relaxed);
          gtm_word locked_by_tx = oreceagerredo_op_mg::set_locked(tx);

          // compute the orecs covering the region being read
          //
          // [mfs] Probably not safe... see pre_write.  We should iterate
          //       over addresses
          size_t orec = oreceagerredo_op_mg::get_orec(addr);
          size_t orec_end = oreceagerredo_op_mg::get_orec_end(addr, len);
          do
          {
              // check orec.
              //
              // [mfs] We need acquire ordering with respect to the
              //       dereference of addr, but this might be overkill to
              //       enforce ordering on every read of an orec.
              gtm_word o = o_oreceagerredo_op_mg.orecs[orec].load(memory_order_acquire);

              // if locked and not too new, log and move to next
              if (likely (!oreceagerredo_op_mg::is_more_recent_or_locked(o, snapshot)))
              {
                success:
                  gtm_rwlog_entry *e = tx->readlog.push();
                  e->orec = o_oreceagerredo_op_mg.orecs + orec;
                  e->value = o;
              }
              // next best: unlocked but too new: extend and then we're good
              // [mfs] I don't like this goto...
              else if (!oreceagerredo_op_mg::is_locked(o))
              {
                  snapshot = extend(tx);
                  goto success;
              }
              // If it's locked, then abort unless we hold the lock...
              else
              {
                  if (o != locked_by_tx)
                      tx->restart(RESTART_LOCKED_READ);
              }
              // advance to next orec
              orec = o_oreceagerredo_op_mg.get_next_orec(orec);
          }
          while (orec != orec_end);
          // NB: returns a pointer to the new readset entries, so that we can
          //     postvalidate them
          return &tx->readlog[log_start];
      }

      // Second pass over orecs, verifying that the we had a consistent read
      static void post_load(gtm_thread *tx, gtm_rwlog_entry* log)
      {
          for (gtm_rwlog_entry *end = tx->readlog.end(); log != end; log++)
          {
              // check each orec in tail of read log
              //
              // [mfs] Note that since we don't deal with incarnation
              //       numbers, we could halve the log size and just compare
              //       to shared_state.
              //
              // [mfs] Note, too, that we could eval all before
              //       restarting... not sure if worth it for this code.
              gtm_word o = log->orec->load(memory_order_relaxed);
              if (log->value != o)
                  tx->restart(RESTART_VALIDATE_READ);
          }
      }

      //[wer210] same as load(), except does not check opslog
      template <typename V> static V promote_load(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          // Read-for-write should be unlikely, but we need to handle it or will
          // break later WaW optimizations.
          //
          // [mfs] I'm curious how much this actually saves... TODO?
          if (unlikely(mod == RfW))
          {
              pre_write(tx, addr, sizeof(V));
              return *addr;
          }

          // we can't trust RaW, since we might not get a hint on a RaW, so
          // do a lookup every time
          //
          // [mfs] This is extremely sub-optimal... we have an O(n) lookup
          //       cost, and we're doing it on every read.  However, we
          //       *know* that if we have a RAW, then we'd also have locked
          //       the orec already.  This code should move down, and we
          //       should check the orec first...
          WriteSetEntry e;
          wslog_insert_data<V>(&e, const_cast<V*>(addr), 0);
          bool found = tx->redolog.find(e);
          // [mfs] Note that this will not be valid when we implement a
          //       solution that properly handles overlapping accesses of
          //       varying granularity.  In that case, the lookup may return
          //       only a *portion* of the total bytes comprising the
          //       returned scalar
          if (found) {
              return wslog_extract_data<V>(e, addr);
          }

          // do the pre-check of orecs... note that is an acquire order
          gtm_rwlog_entry* log = pre_load(tx, addr, sizeof(V));

          // Load the data... there are nasty issues here with ordering, hope
          // that an acquire fence after the load is good enough
          //
          // [mfs] It's probably not for POWER
          V v = *addr;
          atomic_thread_fence(memory_order_acquire);

          // re-check the orec, abort on failure
          post_load(tx, log);

          return v;
      }

      //[wer210] same as store(), except does not check opslog
      template <typename V> static void promote_store(V* addr, const V value,
                                                      ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          // lock orecs unless we know it's a WaW
          //if (likely(mod != WaW))
          //    pre_write(tx, addr, sizeof(V));

          WriteSetEntry e;
          wslog_insert_data<V>(&e, addr, value);
          tx->redolog.insert(e);
      }

      template <typename V> static V op_read(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          //[wer] check if in opslog
          WriteSetEntry e;
          wslog_insert_data<V>(&e, const_cast<V*>(addr), 0);
          if (tx->opslog.remove(e) == true) {
              //promte a read(addr), val += op, write(addr,val)
              V pro_val = promote_load <V> (addr, RfW);
              single_operate_on<V>(&e, &pro_val);
              promote_store <V> (const_cast<V*>(addr), pro_val, WaR);
          }

          // Read-for-write should be unlikely, but we need to handle it or will
          // break later WaW optimizations.
          //
          // [mfs] I'm curious how much this actually saves... TODO?
          if (unlikely(mod == RfW))
          {
              pre_write(tx, addr, sizeof(V));
              return *addr;
          }

          // we can't trust RaW, since we might not get a hint on a RaW, so
          // do a lookup every time
          //
          // [mfs] This is extremely sub-optimal... we have an O(n) lookup
          //       cost, and we're doing it on every read.  However, we
          //       *know* that if we have a RAW, then we'd also have locked
          //       the orec already.  This code should move down, and we
          //       should check the orec first...
          bool found = tx->redolog.find(e);
          // [mfs] Note that this will not be valid when we implement a
          //       solution that properly handles overlapping accesses of
          //       varying granularity.  In that case, the lookup may return
          //       only a *portion* of the total bytes comprising the
          //       returned scalar
          if (found) {
              return wslog_extract_data<V>(e, addr);
          }

          // do the pre-check of orecs... note that is an acquire order
          gtm_rwlog_entry* log = pre_load(tx, addr, sizeof(V));

          // Load the data... there are nasty issues here with ordering, hope
          // that an acquire fence after the load is good enough
          //
          // [mfs] It's probably not for POWER
          V v = *addr;
          atomic_thread_fence(memory_order_acquire);

          // re-check the orec, abort on failure
          post_load(tx, log);

          return v;
      }

      // All _ITM_R* functions ultimately become instances of this
      template <typename V> static V load(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          //[wer] check if in opslog
          WriteSetEntry e;
          wslog_insert_data<V>(&e, const_cast<V*>(addr), 0);
          if (tx->opslog.remove(e) == true) {
              //promte a read(addr), val += op, write(addr,val)
              V pro_val = promote_load <V> (addr, RfW);
              single_operate_on<V>(&e, &pro_val);
              promote_store <V> (const_cast<V*>(addr), pro_val, WaR);
          }

          // Read-for-write should be unlikely, but we need to handle it or will
          // break later WaW optimizations.
          //
          // [mfs] I'm curious how much this actually saves... TODO?
          if (unlikely(mod == RfW))
          {
              pre_write(tx, addr, sizeof(V));
              return *addr;
          }

          // we can't trust RaW, since we might not get a hint on a RaW, so
          // do a lookup every time
          //
          // [mfs] This is extremely sub-optimal... we have an O(n) lookup
          //       cost, and we're doing it on every read.  However, we
          //       *know* that if we have a RAW, then we'd also have locked
          //       the orec already.  This code should move down, and we
          //       should check the orec first...
          bool found = tx->redolog.find(e);
          // [mfs] Note that this will not be valid when we implement a
          //       solution that properly handles overlapping accesses of
          //       varying granularity.  In that case, the lookup may return
          //       only a *portion* of the total bytes comprising the
          //       returned scalar
          if (found) {
              return wslog_extract_data<V>(e, addr);
          }

          // do the pre-check of orecs... note that is an acquire order
          gtm_rwlog_entry* log = pre_load(tx, addr, sizeof(V));

          // Load the data... there are nasty issues here with ordering, hope
          // that an acquire fence after the load is good enough
          //
          // [mfs] It's probably not for POWER
          V v = *addr;
          atomic_thread_fence(memory_order_acquire);

          // re-check the orec, abort on failure
          post_load(tx, log);

          return v;
      }

      template <typename V> static void op_write(V* addr, const V value,
                                                 ls_modifier mod)
      {
      }

      // All _ITM_W functions eventually become instances of this
      template <typename V> static void store(V* addr, const V value,
                                              ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          //[wer210] check if in opslog
          WriteSetEntry e;
          wslog_insert_data<V>(&e, addr, 0);
          if (tx->opslog.remove(e) == true) {
              //promte a read(addr), val += op, write(addr,val)
              V pro_val = promote_load <V> (addr, RfW);
              single_operate_on<V>(&e, &pro_val);
              promote_store <V> (addr, pro_val, WaR);
              mod = WaW;
          }

          // lock orecs unless we know it's a WaW
          if (likely(mod != WaW))
              pre_write(tx, addr, sizeof(V));

          // save the data into the redo log
          //
          // [mfs] Note that the interface, but not behavior, needs to change
          //       when we implement a better write set
          wslog_insert_data<V>(&e, addr, value);
          tx->redolog.insert(e);
      }

      template <typename V> static void ops(V* addr, const V value,
                                            ls_modifier mod, int op)
      {
          gtm_thread *tx = gtm_thr();

          // save the data into redo log
          WriteSetEntry e;
          wslog_insert_data<V>(&e, addr, value);
          e.op = op;
          tx->opslog.commu_insert(e);
      }

    public:
      // [mfs] TODO
      static void memtransfer_static(void *dst, const void* src, size_t size,
                                     bool may_overlap, ls_modifier dst_mod, ls_modifier src_mod)
      {
          assert(false && "[mfs] Not implemented yet\n");

          gtm_rwlog_entry* log = 0;
          gtm_thread *tx = 0;

          if (src_mod == RfW)
          {
              tx = gtm_thr();
              pre_write(tx, src, size);
          }
          else if (src_mod != RaW && src_mod != NONTXNAL)
          {
              tx = gtm_thr();
              log = pre_load(tx, src, size);
          }
          // ??? Optimize for RaR?

          if (dst_mod != NONTXNAL && dst_mod != WaW)
          {
              if (src_mod != RfW && (src_mod == RaW || src_mod == NONTXNAL))
                  tx = gtm_thr();
              pre_write(tx, dst, size);
          }

          // FIXME We should use atomics here (see store()).  Let's just hope that
          // memcpy/memmove are good enough.
          if (!may_overlap)
              ::memcpy(dst, src, size);
          else
              ::memmove(dst, src, size);

          // ??? Retry the whole memtransfer if it wasn't consistent?
          if (src_mod != RfW && src_mod != RaW && src_mod != NONTXNAL)
          {
              // See load() for why we need the acquire fence here.
              atomic_thread_fence(memory_order_acquire);
              post_load(tx, log);
          }
      }

      // [mfs] TODO
      static void memset_static(void *dst, int c, size_t size, ls_modifier mod)
      {
          assert(false && "[mfs] Not implemented yet\n");
          if (mod != WaW) {
              gtm_thread *tx = gtm_thr();
              pre_write(tx, dst, size);
          }

          // FIXME We should use atomics here (see store()).  Let's just hope that
          // memset is good enough.
          ::memset(dst, c, size);
      }

      // basic code for starting a transaction
      virtual gtm_restart_reason begin_or_restart()
      {
          // for flat nested transactions, just return...
          //
          // [mfs] TODO: verify that we only have flat nesting, not closed
          //             nesting...
          gtm_thread *tx = gtm_thr();
          if (tx->parent_txns.size() > 0)
              return NO_RESTART;

          // Use an ordered read to get transaction start time
          gtm_word snapshot = o_oreceagerredo_op_mg.time.load(memory_order_acquire);
          // deal with time overflow
          if (snapshot >= o_oreceagerredo_op_mg.TIME_MAX)
              return RESTART_INIT_METHOD_GROUP;

          // save start time (no ordering needed)
          tx->shared_state.store(snapshot, memory_order_relaxed);
          return NO_RESTART;
      }

      // Code for committing a transaction
      virtual bool trycommit(gtm_word& priv_time)
      {
          gtm_thread* tx = gtm_thr();

          // read-only transactions just clear their read log and they're
          // done.  Note that we assume readers don't privatize, so priv_time
          // isn't set
          if (!tx->writelog.size()) {
              if (!tx->opslog.size()) {
                  tx->readlog.clear();
                  return true;
              }
          }

          // Get a commit time via increment of counter
          gtm_word ct = o_oreceagerredo_op_mg.time.fetch_add(1, memory_order_acq_rel) + 1;

          // validate if anyone committed since our start time
          gtm_word snapshot = tx->shared_state.load(memory_order_relaxed);
          if (snapshot < ct - 1 && !validate(tx))
              return false;

          if (tx->opslog.size()) {
              // grep locks
              for (WriteSetEntry *i = tx->opslog.begin(), *e = tx->opslog.end(); i != e; ++i) {
                  if (pre_ops(tx, i->addr.vp, i->size()) == false) {
                      return false;
                  }
              }

              tx->redolog.writeback();
              tx->opslog.operate();
              tx->opslog.reset();
          }
          else
              // replay redo log
              tx->redolog.writeback();

          // release orecs
          //
          // [mfs] Ordering seems excessive
          gtm_word v = oreceagerredo_op_mg::set_time(ct);
          for (gtm_rwlog_entry *i = tx->writelog.begin(), *ie = tx->writelog.end();
               i != ie; i++)
              i->orec->store(v, memory_order_release);

          // clear logs
          tx->readlog.clear();
          tx->redolog.reset();
          tx->writelog.clear();

          // To support quiescence, return our commit time
          priv_time = ct;
          return true;
      }

      // this code runs on abort
      virtual void rollback(gtm_transaction_cp *cp)
      {
          // assuming flat nesting, we just return on abort of nested transaction
          if (cp != 0)
              return;

          gtm_thread *tx = gtm_thr();

          // Release orecs by resetting to their old values
          for (gtm_rwlog_entry *i = tx->writelog.begin(), *ie = tx->writelog.end();
               i != ie; i++)
          {
              i->orec->store(i->value, memory_order_release);
          }

          // [mfs] I'm not convinced that we need this ordering in lazy STM
          atomic_thread_fence(memory_order_release);

          // We're done, clear the logs.
          tx->writelog.clear();
          tx->readlog.clear();
          tx->redolog.reset();
          tx->opslog.reset();
      }

      virtual bool supports(unsigned number_of_threads)
      {
          // Each txn can commit and fail and rollback once before checking for
          // overflow, so this bounds the number of threads that we can support.
          // In practice, this won't be a problem but we check it anyway so that
          // we never break in the occasional weird situation.
          return (number_of_threads * 2 <= oreceagerredo_op_mg::OVERFLOW_RESERVE);
      }

      CREATE_DISPATCH_METHODS(virtual, )
      CREATE_DISPATCH_METHODS_MEM()

      oreceagerredo_op_dispatch() : abi_dispatch(false, true, false, false, 0, &o_oreceagerredo_op_mg)
      { }
  };

} // anon namespace

static const oreceagerredo_op_dispatch o_oreceagerredo_op_dispatch;

abi_dispatch *
GTM::dispatch_oreceagerredo_op ()
{
    return const_cast<oreceagerredo_op_dispatch *>(&o_oreceagerredo_op_dispatch);
}
