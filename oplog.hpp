#ifndef OPLOG_HPP__
#define OPLOG_HPP__

#include "WriteSetEntry.hpp"
#include "containers.h"

namespace GTM HIDDEN{

  struct OpLog
  {
      vector<WriteSetEntry> logs;

      void insert(WriteSetEntry* entry) {
          WriteSetEntry* new_entry = logs.push(1);
          memcpy(new_entry, entry, sizeof(WriteSetEntry));

      }

      inline void operate() {
          for (WriteSetEntry* i = logs.begin(), *ie = logs.end(); i != ie; i++) {
              i->operate();
          }

      }

      // we don't operate on entris that are deleted
      inline void operate_fine() {
          for (WriteSetEntry* i = logs.begin(), *ie = logs.end(); i != ie; i++) {
              if (i->op != OP_DELETED) i->operate();
          }
      }

      inline bool exist(const void* addr)
      {
          for (WriteSetEntry* i = logs.begin(), *e = logs.end(); i != e; i++) {
              if (i->addr.vp == addr){
                  if ((i->op == OP_P || i->op == OP_S ) && (i->val.ull != 0))
                      return true;
                  if ((i->op == OP_M || i->op == OP_D ) && (i->val.ull != 1))
                      return true;
              }
          }
          return false;
      }

      inline WriteSetEntry* get_exist(const void* addr)
      {
          for (WriteSetEntry* i = logs.begin(), *e = logs.end(); i != e; i++) {
              if (i->addr.vp == addr && i->op != OP_DELETED){
                      return i;
              }
          }
          return NULL;
      }

      inline bool exist_fine(const void* addr)
      {
          for (WriteSetEntry* i = logs.begin(), *e = logs.end(); i != e; i++) {
              if (i->addr.vp == addr && i->op != OP_DELETED)
                      return true;
          }
          return false;
      }
#if 0
      void remove(WriteSetEntry& log) const
      {
          void* addr = log.addr.vp;
          for (WriteSetEntry* i = logs.begin(), *e = logs.end(); i != e; i++) {
              if (i->addr.vp == log.addr.vp) {
                  switch (i->op) {
                    case OP_P: i->val.vp = 0; break;
                    case OP_S: i->val.vp = 0; break;
                    default:
                      i->val.vp = 1; // for * and /
                  };
              }
          }
      }
#endif

      WriteSetEntry* begin() const { return logs.begin(); }
      WriteSetEntry* end() const { return logs.end(); }
      void reset() { logs.clear(); }
      size_t size() const { return logs.size(); }

  };

}
#endif
