/* Copyright (C) 2012 Free Software Foundation, Inc.
   Contributed by Torvald Riegel <triegel@redhat.com>.

   This file is part of the GNU Transactional Memory Library (libitm).

   Libitm is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Libitm is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   Under Section 7 of GPL version 3, you are granted additional
   permissions described in the GCC Runtime Library Exception, version
   3.1, as published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License and
   a copy of the GCC Runtime Library Exception along with this program;
   see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
   <http://www.gnu.org/licenses/>.  */

#include "libitm_i.h"
#include <stdio.h>

#if 0
#  define CFENCE              __asm__ volatile ("":::"memory")
#  define WBR                 __sync_synchronize()
#  define faiptr(p)           __sync_fetch_and_add(p, 1)
#  define bcasptr(p, o, n)    __sync_bool_compare_and_swap(p, o, n)
#  define faaptr(p, a)        __sync_fetch_and_add(p, a)
#endif

// CohortsNOrec

using namespace GTM;

namespace {
#if 0
  struct volatile_t {
      volatile uint32_t val;
      char padding [HW_CACHELINE_SIZE - sizeof (uint32_t)];
  };
#endif

  // method group with all the operations for method-cohorts
  struct cohorts_mg : public method_group
  {
      // method-wide definitions.  We have as much overlap with method-ml as
      // is reasonable, deviating only when there is an algorithmic need
      //
      // NB: some of this code is unnecessary, but is preserved for the sake
      //     of consistency with method-ml.  Note, too, that method-ml has
      //     more documentation on /why/ these are implemented as they are.

      // methods for managing lock bits and overflow of the global timebase
      //
      // NB: There are no incarnation bits for this algorithm, since we don't
      //     have undo logs

      //[wer210] shared counters for Cohorts.
      atomic <gtm_word> cpending __attribute__ ((aligned(HW_CACHELINE_SIZE)));
      atomic <gtm_word> started  __attribute__ ((aligned(HW_CACHELINE_SIZE)));
      atomic <gtm_word> committed __attribute__ ((aligned(HW_CACHELINE_SIZE)));

      static const gtm_word TIME_MAX = (~(gtm_word)0 >> 2);
      static const gtm_word OVERFLOW_RESERVE = TIME_MAX + 1;
#if 0
      volatile_t cpending;
      volatile_t started;
      volatile_t committed;
      volatile_t last_complete;
#endif
      // The shared time base.
      atomic<gtm_word> time __attribute__((aligned(HW_CACHELINE_SIZE)));

      virtual void init()
      {
          // for now, we're going to print a message to confirm algorithm selection
          printf("Initializing cohorts algorithm\n");
          // NB: this code is called while holding the serial lock
          time.store(0, memory_order_relaxed);

          cpending.store (0, memory_order_relaxed);
          committed.store (0, memory_order_relaxed);
          started.store (0, memory_order_relaxed);
      }

      virtual void fini()
      {
      }

      // We only re-initialize when our time base overflows.  Thus, only reset
      // the time base and the orecs but do not re-allocate the orec array.
      //
      // [mfs] This should never happen in 64-bit code...
      virtual void reinit()
      {
          // This store is only executed while holding the serial lock, so relaxed
          // memory order is sufficient here.  Same holds for the memset.
          time.store(0, memory_order_relaxed);

          cpending.store (0, memory_order_relaxed);
          committed.store (0, memory_order_relaxed);
          started.store (0, memory_order_relaxed);
      }
  };

  // [mfs] This is our method group object.  Note that GCC does not have
  //       per-thread pointers.
  static cohorts_mg o_cohorts;

  // The multiple lock, write-back TM method
  //
  // As discussed above, this should be thought of as (RSTM OrecLazy) +
  // (Privatization Safety via Quiescence).  In particular, this means eager
  // locking with write back.  We never expect this to be a good algorithm.
  //
  // Reads use time-based validation with timestamp extension.
  //
  // gtm_thread::shared_state is used to store a transaction's current
  // snapshot time (or commit time). The serial lock uses ~0 for inactive
  // tranasactions and 0 for active ones. Thus, we always have a meaningful
  // timestamp in shared_state that can be used to implement quiescence-based
  // privatization safety.
  class cohorts_dispatch : public abi_dispatch
  {
    protected:
      static bool validate(gtm_thread *tx)
      {
          return tx->valuelog_bst.valuecheck();
      }

      template <typename V> static V op_read(const V* addr, ls_modifier mod)
      {
          assert (false && "This is cohorts, use cohorts_op instead.");
          V v = *addr;
          return v;
      }

      template <typename V> static void op_write(V* addr, const V value,
                                                 ls_modifier mod)
      {
          assert (false && "This is cohorts, use cohorts_op instead.");
      }


      // All _ITM_R* functions ultimately become instances of this
      template <typename V> static V load(const V* addr, ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          V v;
          if (!tx->redolog_bst.isEmpty() && tx->redolog_bst.find(addr, v) != 0)
              return v;

          v = *addr;
          tx->valuelog_bst.insert (addr, v);
          return v;
      }

      // All _ITM_W* functions eventually become instances of this
      template <typename V> static void store(V* addr, const V value,
                                              ls_modifier mod)
      {
          gtm_thread *tx = gtm_thr();

          // save the data into the redo log
          tx->redolog_bst.insert(addr, value);
      }

      template <typename V> static void ops(V* addr, const V value,
                                            ls_modifier mod, int op)
      {
          assert (false && "This is cohorts, use cohorts_op instead.");
      }

    public:
      // [mfs] TODO
      static void memtransfer_static(void *dst, const void* src, size_t size,
                                     bool may_overlap, ls_modifier dst_mod, ls_modifier src_mod)
      {
          //It's not optimal, but it should at least work

          //[wer] copy "num of size" bytes, so simply treat the type as unsigned char
          // we also do not care about overlap, as we do buffered write.
          unsigned char* srcaddr = (unsigned char*)const_cast<void*>(src);
          unsigned char* dstaddr = (unsigned char*)dst;

          // load and store
          for (size_t i = 0; i < size; i++) {
              unsigned char temp = load<unsigned char>(srcaddr, RaR);
              store<unsigned char>(dstaddr, temp, WaW);
              dstaddr = (unsigned char*) ((long long)dstaddr + sizeof(unsigned char));
              srcaddr = (unsigned char*) ((long long)srcaddr + sizeof(unsigned char));
              //dstaddr++;
              //srcaddr++;
          }
      }

      // [mfs] TODO
      static void memset_static(void *dst, int c, size_t size, ls_modifier mod)
      {
          gtm_thread* tx = gtm_thr();
          unsigned char* dstaddr = (unsigned char*)dst;

          // save data into redo log
          for(size_t it = 0; it < size; it++) {
              tx->redolog_bst.insert(dstaddr, (unsigned char)c);
              dstaddr = (unsigned char*) ((long long)dst + sizeof(unsigned char));
              //dstaddr++;
          }
      }

      // basic code for starting a transaction
      virtual gtm_restart_reason begin_or_restart()
      {
          // for flat nested transactions, just return...
          //
          // [mfs] TODO: verify that we only have flat nesting, not closed
          //             nesting...
          gtm_thread *tx = gtm_thr();
          if (tx->parent_txns.size() > 0)
              return NO_RESTART;

          // Use an ordered read to get transaction start time
          gtm_word snapshot = o_cohorts.time.load(memory_order_relaxed);

          // deal with time overflow
          if (snapshot >= o_cohorts.TIME_MAX)
              return RESTART_INIT_METHOD_GROUP;

      S1:
          // wait until everyone is committed
          while (o_cohorts.cpending.load(memory_order_relaxed)
                 != o_cohorts.committed.load(memory_order_relaxed));

          // before tx begins, increase total number of tx
          o_cohorts.started.fetch_add(1, memory_order_acq_rel);

          if (o_cohorts.cpending.load(memory_order_relaxed)
              > o_cohorts.committed.load(memory_order_relaxed)) {
              o_cohorts.started.fetch_add(-1, memory_order_acq_rel);
              goto S1;
          }

          return NO_RESTART;
      }

      // Code for committing a transaction
      virtual bool trycommit(gtm_word& priv_time)
      {
          gtm_thread* tx = gtm_thr();

          // read-only transactions just clear their read log and they're
          // done.  Note that we assume readers don't privatize, so priv_time
          // isn't set
          if (tx->redolog_bst.isEmpty()) {
              o_cohorts.started.fetch_add(-1, memory_order_acq_rel);
              tx->valuelog_bst.reset();
              return true;
          }

          gtm_word first = o_cohorts.time.load(memory_order_relaxed) + 1;
          // get the order
          tx->order = 1 + o_cohorts.cpending.fetch_add(1, memory_order_acq_rel);

          // wait for my turn
          while (o_cohorts.time.load(memory_order_relaxed) != (uintptr_t)(tx->order - 1));

          // get the lock and validate
          if ( (uintptr_t)tx->order != first)
              if (!validate(tx)) {
                  o_cohorts.committed.fetch_add(1, memory_order_relaxed);
                  o_cohorts.time.store(tx->order, memory_order_release);
                  tx->restart(RESTART_VALIDATE_READ);
              }

          //Wait until all tx are ready to commit
          while (o_cohorts.cpending.load(memory_order_relaxed)
                 < o_cohorts.started.load(memory_order_relaxed));

          // replay redo log
          tx->redolog_bst.writeback();

          // increase total number of committed tx
          //[wer210] TODO: verify the ordering here.
          o_cohorts.committed.fetch_add(1, memory_order_release);
          o_cohorts.time.store(tx->order, memory_order_release);

          // clear logs
          tx->redolog_bst.reset();
          tx->valuelog_bst.reset();
          return true;
      }

      // this code runs on abort
      virtual void rollback(gtm_transaction_cp *cp)
      {
          // assuming flat nesting, we just return on abort of nested transaction
          if (cp != 0)
              return;

          gtm_thread *tx = gtm_thr();

          // We're done, clear the logs.
          tx->redolog_bst.reset();
          tx->valuelog_bst.reset();
      }

      virtual bool supports(unsigned number_of_threads)
      {
          // Each txn can commit and fail and rollback once before checking for
          // overflow, so this bounds the number of threads that we can support.
          // In practice, this won't be a problem but we check it anyway so that
          // we never break in the occasional weird situation.
          return (number_of_threads * 2 <= cohorts_mg::OVERFLOW_RESERVE);
      }

      CREATE_DISPATCH_METHODS(virtual, )
      CREATE_DISPATCH_METHODS_MEM()

      cohorts_dispatch() : abi_dispatch(false, true, false, false, 0, &o_cohorts)
      { }
  };

} // anon namespace

static const cohorts_dispatch o_cohorts_dispatch;

abi_dispatch *
GTM::dispatch_cohorts ()
{
    return const_cast<cohorts_dispatch *>(&o_cohorts_dispatch);
}
